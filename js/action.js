/*
Сделать массив пользователей 
User -> id, name, age, email, image
пробежать циклом по массиву и отрисовать пользователей
*/

const users = [];
getUsers();
drawUser();

const addBtn = document.querySelector('#add');
addBtn.addEventListener('click', addUser);

document.addEventListener('keydown', checkDelKey);

const inputs = Array.from(document.querySelectorAll('input'));
onFocusInputs();

function onFocusInputs() {
    inputs.forEach(item => {
        item.onfocus = function() {
            item.addEventListener('keydown', toggleInput);
        }
    });
}

function toggleInput(event) {
    if (event.keyCode === 13) {
        const inputIndex = inputs.indexOf(event.target);

        if (inputIndex !== inputs.length - 1) {
            inputs[inputIndex + 1].focus();
        } else {
            inputs[0].focus();
        }
    }
}

function checkDelKey(event) {
    if(event.keyCode === 46) {
        deleteLastUser();
    }
}

function deleteLastUser() {
    users.pop();
    rewriteStorage();
    drawUser();
}

function getUsers() {
    const usersStorgae = JSON.parse(localStorage.getItem('users'));

    if (usersStorgae) {
        users.push(...usersStorgae);
    }
}

function drawUser() {
    const containerList = document.querySelector('.container__list');
    containerList.innerHTML = '';
    users.forEach(item => containerList.appendChild(createUser(item)));
}

function createUser(users) {
    let userName = document.createElement('span');
    userName.className = 'user__name';
    userName.innerText = users.name;

    let userAge = document.createElement('span');
    userAge.className = 'user__age';
    userAge.innerText = users.age;

    let userEmal = document.createElement('a');
    userEmal.href = '#';
    userEmal.className = 'user__email';
    userEmal.innerText = users.email;

    let br = document.createElement('br');

    let userContent = document.createElement('div');
    userContent.className = 'user__content';
    userContent.appendChild(userName);
    userContent.appendChild(userAge);
    userContent.appendChild(br);
    userContent.appendChild(userEmal);

    const userImg = document.createElement('img');
    userImg.classList.add('user__img');
    userImg.height = '185';
    userImg.width = '111';
    userImg.src = users.file;

    const deleteBtn = document.createElement('div');
    deleteBtn.className = 'user__delete';
    deleteBtn.id = 'del_' + users.id;
    deleteBtn.innerText = 'x';
    deleteBtn.addEventListener('click', deleteUser);

    let user = document.createElement('div');
    user.className = 'user';
    user.appendChild(userImg);
    user.appendChild(userContent);
    user.appendChild(deleteBtn);

    return user;
}

function deleteUser(event) {
    const delId = event.target.id;
    console.log(delId)
    const userId = Number(delId.split('_')[1]);
    console.log(userId)
    const userIds = users.map(item => item.id);
    console.log(userIds)
    const userIndex = userIds.indexOf(userId);
    users.splice(userIndex, 1);
    drawUser();
    rewriteStorage();
}

function addUser() {
    const nameInput = document.querySelector('#userName');
    const newUserName = nameInput.value;
    nameInput.value = '';

    const ageInput = document.querySelector('#userAge');
    const newUserAge = ageInput.value;
    ageInput.value = '';

    const emailInput = document.querySelector('#userEmail');
    const newUserEmail = emailInput.value;
    emailInput.value = '';

    const fileInput = document.querySelector('#userPhoto');
    const selectedFile = fileInput.files[0];

    if (selectedFile) {
        (function(file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
    
            reader.onload = function () {
                const base64 = reader.result;
    
                const newUser = {
                    id: getNewId(),
                    name: newUserName,
                    age: newUserAge,
                    email: newUserEmail,
                    file: base64,
                }
    
                users.push(newUser);
                rewriteStorage();
                drawUser(); 
            };
    
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        })(selectedFile);
    } else {
        const base64 = '';
    
            const newUser = {
                id: getNewId(),
                name: newUserName,
                age: newUserAge,
                email: newUserEmail,
                file: base64,
            }
    
            users.push(newUser);
            rewriteStorage();
            drawUser(); 
    }

    nameInput.focus();
}

function rewriteStorage() {
    localStorage.setItem('users', JSON.stringify(users));
}

function getNewId() {
    const ids = users.map(item => item.id);
    let maxId;

    if (ids.length) {
        maxId = Math.max(...ids) + 1;
    } else {
        maxId = 0;
    }

    return maxId;
}










